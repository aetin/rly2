#compile
CC=gcc

#flags
CFLAGS+= -Wall -Wshadow -pedantic -g

#exe name
EXE=RLY02.x

#ojects
OBJ = rly2_driver.o

all: $(EXE)

clean: 
	rm -f *.o *~

dclean: clean
	rm -f *.x


$(EXE): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

#regles
%.o: %.c
	$(CC) $(CFLAGS) -c $<
