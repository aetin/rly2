/**
* Driver for the USB-RLY02 - 2 relay outputs at 16A
* copyright (c) antlas
*/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include "rly2_driver.h"


static struct option long_options[] = {
    {"up",      no_argument,    0,  'u' },
    {"down",	no_argument,    0,  'd' },
    {"one",    	no_argument,	0,  'o' },
    {"two",    	no_argument, 	0,  't' },
    {"all",    	no_argument, 	0,  'a' }
};

int g_action = -1;
int g_relay = -1;

int main(int argc, char** argv)
{
	int fd = -1;

	parse_options(argc, argv);
    
	fd = open_port();
	if(fcntl(fd, F_GETFD) != -1)
	{
		command(fd);
		close_port(fd);
	}

	return 0;
}

int parse_options(int argc, char** argv)
{
	int long_index = 0;
	int opt = 0;

    while ((opt = getopt_long(argc, argv,"udota", 
                   long_options, &long_index)) != -1) {
        switch (opt) {
             case 'u' :
             	g_action = RLY2_ACTION_ON;
                break;
             case 'd' :
             	g_action = RLY2_ACTION_OFF;
             	break;
             case 'o' :
             	g_relay  = RLY2_ONE; 
                break;
             case 't' :
             	g_relay  = RLY2_TWO;
                break;
             case 'a' :
             	g_relay  = RLY2_ALL;
                break;
             default: print_usage(); 
                exit(EXIT_FAILURE);
                break;
        }
    }
    return 0;
}


int command(int fd)
{
	int flag = 0x0;
	int ret = -1;
	
	flag = g_action + g_relay; 
	ret = write(fd, &flag, 1);
	
	return ret;
}

int open_port(void)
{
	struct termios options;
	int fd = -1;
	struct timespec time_to_sleep, t2;
	time_to_sleep.tv_sec  = 0;
	time_to_sleep.tv_nsec = 500000L;

	fd = open(RLY2_TTY_PATH, O_RDWR | O_NOCTTY | O_NDELAY);

	if (fd ==-1)
  	{
    	printf("[E]: cant open port\n");
    	exit(EXIT_FAILURE);
  	}

    fcntl(fd,F_SETFL,0); /* set */
    tcgetattr(fd,&options); /* get terminal configuration */

    nanosleep(&time_to_sleep, &t2);

    cfsetospeed(&options,B115200);
    cfsetispeed(&options, B115200); /* */
    options.c_cflag &= ~PARENB; /* Parite   : none */
    options.c_cflag &= ~CSTOPB; /* Stop bit : 1    */
    options.c_cflag &= ~CSIZE;  /* Bits     : 8    */
    options.c_cflag |= CS8;
    /*options.c_cflag &= ~CRTSCTS; // to disable RTS-CTS */
    options.c_oflag &= ~OPOST;
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    tcsetattr(fd,TCSANOW,&options);  /* initialisation la ligne avec les nouveaux parametres */

    nanosleep(&time_to_sleep, &t2);

  	return fd;
}

int close_port(int fd)
{
	if(close(fd) != 0)
		printf("[E]: closing connection\n");

	return 0;
}

int print_usage()
{
	printf("usage: -[u|d][o|t|]>\n");

	return 0;
}
