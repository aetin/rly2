/**
* Driver for the USB-RLY02 - 2 relay outputs at 16A
* copyright (c) antlas
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include <fcntl.h>
#include <assert.h>
#include <pthread.h>

#define RLY2_TTY_PATH "/dev/ttyACM0"

/* get software version*/
#define RLY2_GET_SOFT_VER 0x5A
/* get relay states*/
#define RLY2_GET_STATE 0x5B
/* set relay states*/
#define RLY2_SET_STATE 0x5C

/* all relays */
#define RLY2_ALL 0x0
/* relay 1 */
#define RLY2_ONE 0x1
/* relay 2 */
#define RLY2_TWO 0x2

/* action ON*/
#define RLY2_ACTION_ON 0x64
/* action OFF*/
#define RLY2_ACTION_OFF 0x6E


int parse_options(int argc, char** argv);
int open_port(void);
int command(int);
int close_port(int);
int print_usage();
