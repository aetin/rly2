# USB RLY02 - Robot Electronics driver
##### How to build
* Ensuring you have gcc on your host machine, call

```bash
$ make
``` 

##### How to run
```bash
# activate relay one
$ RLY02.x -uo

# activate relay two
$ RLY02.x -ut

# activate both relays
$ RLY02.x -ua

# deactivate relay one
$ RLY02.x -do

# deactivate relay two
$ RLY02.x -dt

# deactivate both relays
$ RLY02.x -da
```
